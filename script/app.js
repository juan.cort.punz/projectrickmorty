window.onload = () => {

    createHeader();
    createMain();

};


const data = {
    option: [
        {
            title: "Personajes",
            url: "https://rickandmortyapi.com/api/character"

        },
        {
            title: "Lugares",
            url: "https://rickandmortyapi.com/api/location"

        },
        {
            title: "Episodios",
            url: "https://rickandmortyapi.com/api/episode"

        },
    ],
};

function createHeader() {

    const navbar$$ = document.querySelector('.navbar');
    const navbarnav$$ = document.querySelector('.navbar-nav');
    const head$$ = document.createElement('header');
    const h1$$ = document.createElement('h1');
    const a$$ = document.createElement('a');
    a$$.classList.add('a-h1');
    a$$.setAttribute('href', 'index.html');
    h1$$.classList.add('f-h1', 'animate__animated', 'animate__backInLeft')
    a$$.textContent = 'Rick and Morty'
    head$$.appendChild(h1$$);
    h1$$.appendChild(a$$);
    navbarnav$$.before(navbar$$.appendChild(head$$))

}

function createMain() {

    const main$$ = document.querySelector('main');
    const div$$ = document.createElement('div');
    div$$.classList.add('list-e');
    const a$$ = document.querySelectorAll('.nav-item');

    for (let i = 0; i < data.option.length; i++) {
        a$$[i].addEventListener('click', function (e) {

            clearContainer();
            e.preventDefault();
            createContent(data.option[i]);
        })
    }


    main$$.appendChild(div$$);
}

function createContent(date) {

    const ul$$ = document.createElement('ul');
    ul$$.classList.add('animate__animated', 'animate__backInUp');

    fetch(date.url).then(res => res.json())
        .then((data) => {

            for (let i = 0; i < data.results.length - 8; i++) {
                const div$$ = document.querySelector('.list-e');
                const li$$ = document.createElement('li');
                const a$$ = document.createElement('a');
                a$$.textContent = data.results[i].name;
                li$$.appendChild(a$$);
                a$$.setAttribute('href', '#')
                ul$$.appendChild(li$$);
                div$$.appendChild(ul$$);
                a$$.addEventListener('click', function (e) {

                    if (date.title == 'Personajes') {
                        clearContainer();
                        createCardCharacter(data.results[i], createCard(), btnClose(date));

                    } else if (date.title == 'Lugares') {
                        clearContainer();
                        e.preventDefault();
                        createCardLocation(data.results[i], createCard(), btnClose(date));

                    } else {
                        clearContainer();
                        e.preventDefault();
                        createCardChapter(data.results[i], createCard(), btnClose(date));
                    }


                })
            }

        })
}


function createCard() {

    const div$$ = document.querySelector('.list-e');
    const card$$ = document.createElement('card');

    card$$.classList.add('card');
    card$$.classList.add('animate__animated', 'animate__backInUp');
    card$$.setAttribute('style', 'width: 18rem');
    div$$.appendChild(card$$);
    return card$$;
}

function btnClose(date) {

    const btn$$ = document.createElement('a');
    btn$$.classList.add('a-close')
    btn$$.setAttribute('href', '#');
    const img$$ = document.createElement('img');
    img$$.setAttribute('src', '/img/close-icon.png')
    btn$$.appendChild(img$$);

    setTimeout(() => {
        const bcl = document.querySelector('.a-close');
        bcl.addEventListener('click', function () {

            clearContainer();
            createContent(date);
        }, 1000);
    })

    return btn$$;

}


function createCardCharacter(date, createCard, btnClose) {

    const img$$ = document.createElement('img');
    img$$.setAttribute('src', date.image);
    createCard.appendChild(img$$);

    const cBody$$ = document.createElement('div');
    cBody$$.classList.add('card-body');
    createCard.appendChild(cBody$$);
    const h5$ = document.createElement('h5');
    h5$.textContent = date.name;
    cBody$$.appendChild(h5$);
    const pOne$ = document.createElement('p');
    pOne$.textContent = 'Especie: ' + date.species;
    cBody$$.appendChild(pOne$);
    const pTwo$ = document.createElement('p');
    pTwo$.textContent = 'Género: ' + date.gender;
    cBody$$.appendChild(pTwo$);
    const pThr$ = document.createElement('p');
    pThr$.textContent = 'Estado: ' + date.status;
    cBody$$.appendChild(pThr$);
    cBody$$.appendChild(btnClose);

}

function createCardLocation(date, createCard, btnClose) {

    const img$$ = document.createElement('img');
    img$$.setAttribute('src', '/img/logo.png');
    createCard.appendChild(img$$);

    const cBody$$ = document.createElement('div');
    cBody$$.classList.add('card-body');
    createCard.appendChild(cBody$$);
    const h5$ = document.createElement('h5');
    h5$.textContent = date.name;
    cBody$$.appendChild(h5$);
    const pOne$ = document.createElement('p');
    pOne$.textContent = 'Tipo: ' + date.type;
    cBody$$.appendChild(pOne$);
    const pTwo$ = document.createElement('p');
    pTwo$.textContent = 'Dimensión: ' + date.dimension;
    cBody$$.appendChild(pTwo$);
    const pThr$ = document.createElement('p');
    pThr$.textContent = 'Creado: ' + date.created;
    cBody$$.appendChild(pThr$);
    cBody$$.appendChild(btnClose);
}



function createCardChapter(date, createCard, btnClose) {

    const img$$ = document.createElement('img');
    img$$.setAttribute('src', '/img/logo.png');
    createCard.appendChild(img$$);

    const cBody$$ = document.createElement('div');
    cBody$$.classList.add('card-body');
    createCard.appendChild(cBody$$);
    const h5$ = document.createElement('h5');
    h5$.textContent = date.name;
    cBody$$.appendChild(h5$);
    const pOne$ = document.createElement('p');
    pOne$.textContent = 'Emitido: ' + date.air_date;
    cBody$$.appendChild(pOne$);
    const pTwo$ = document.createElement('p');
    pTwo$.textContent = 'Episodio: ' + date.episode;
    cBody$$.appendChild(pTwo$);
    const pThr$ = document.createElement('p');
    pThr$.textContent = 'Creado: ' + date.created;
    cBody$$.appendChild(pThr$);
    cBody$$.appendChild(btnClose);

}

function clearContainer() {
    document.querySelector('.list-e').innerHTML = '';
}



