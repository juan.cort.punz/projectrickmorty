window.onload = () => {
    
createHeader();
navBar();
contentMain();
/* changeBackground(); */

};
const data = {
    option: [
        {
        title: "Personajes",
        url: "https://rickandmortyapi.com/api/character"
    
        }, 
        {
            title: "Lugares",
            url: "https://rickandmortyapi.com/api/location"
        
        },
        {
            title: "Episodios",
            url: "https://rickandmortyapi.com/api/episode"
        
        }, 
        ],
    };

function createAnchor(txt , callback){
    const a$$ = document.createElement('a');
    const txt$$ = document.createTextNode(txt);
    a$$.appendChild(txt$$);
    a$$.setAttribute('href', '#');

    a$$.addEventListener('click', function(e){
        e.preventDefault();
        callback()
    });
    return a$$;
}    

function createHeader(){
    const main = document.querySelector('main');
    const head$$ = document.createElement('header');
    const h1$$ = document.createElement('h1');
    h1$$.classList.add('f-h1');
    h1$$.textContent = 'Rick and Morty'
    head$$.appendChild(h1$$);
    main.appendChild(head$$);
    
}

function navBar(){
    const main$$ = document.querySelector('main');
    const nav$$ = document.createElement('div');
    nav$$.classList.add('nav-bar')
    main$$.appendChild(nav$$);
    
    for (let i = 0; i < data.option.length; i++) {
        const a$$ = createAnchor(data.option[i].title, function(){
            clearContainer();
            createContent(data.option[i].title);
            createTable(data.option[i].url);
        })
        nav$$.appendChild(a$$);
    }
   
}

function contentMain(){
    const main$$ = document.querySelector('main');
    const cont$$ = document.createElement('div');
    cont$$.classList.add('content')
    main$$.appendChild(cont$$);
    const art$$ = document.createElement('article');
    cont$$.appendChild(art$$);
}

function createContent(txt){
    const art$$ = document.querySelector('article');
    const h1$$ = document.createElement('h1');
    const h1txt$$ = document.createTextNode(txt);
    h1$$.appendChild(h1txt$$);
    art$$.appendChild(h1$$);
}



function createTable(url){
    fetch(url).then(res => res.json())
    .then(function(date){
        const art$$ = document.querySelector('article');
        const ul$$ = document.createElement('ul');

        for (let i = 0; i < date.results.length-13; i++) {
            const li$$ = document.createElement('li');
            const a$$ = createAnchor(date.results[i].name, function(){
                clearContainer();
                
            })

            li$$.appendChild(a$$);
            ul$$.appendChild(li$$);
            art$$.appendChild(ul$$);
            
        }
    })
}

/* function createCard(){
    const card$$ = document.createElement('div');
    card$$.classList.add('cards');
    document.body.appendChild(card$$);
    const val = document.querySelectorAll('a');
        
        for (let i = 0; i < val.length; i++) {
            
            val[i].addEventListener('click', function(e){
                let pos = e.target.textContent;
                
                for (let z = 0; z < data.option.length; z++) {
                    if (data.option[z].title == pos) {
                        fetch(data.option[z].url).then(res => res.json())
                        .then(function(date){
                            const car$$ = document.querySelector('.cards');
                            
                            for (let w = 0; w < date.results.length-15; w++) {
                                const dCard$$ = document.createElement('div');
                                dCard$$.classList.add('card'+ data.option[z].title);
                                car$$.appendChild(dCard$$)
                                
                            }
                            
                            
                    })
                    }
                    
                }
                e.preventDefault();
                /* fetch(data.option[0].url).then(res => res.json())
                .then(function(data){
                    console.log(data.results)
                }) */
        /*     })
        }
    
} */ 

function clearContainer(){
    document.querySelector('article').innerHTML = '';
}

/* function toggleDiv(){
const pers$$ = document.querySelector('.cardPersonajes');
const lug$$ = document.querySelector('.cardLugares');
const epi$$ = document.querySelector('.cardEpisodios');
if (pers$$) {
    pers$$.style.display= 'none';
}
 */



